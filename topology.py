import os
import sys
sys.path.insert(0, '../mininetInstall/mininet')
import time
import random
import threading
from mininet.cli import CLI
from mininet.net import Mininet
from mininet.node import RemoteController

#This function automaticly generates topology of mininet based on the input file
def buildNetworkTopology(switchIds, hostIds, switchLinksProperties, hostLinksProperties):
    global net
    net = Mininet(controller=RemoteController)
    print("Adding Controller...")
    net.addController(name='c0', port=6633)
    print("Adding Switches...")
    for switchId in switchIds:
        net.addSwitch('sw' + str(switchId[0]), mac=switchId[1].strip())
    print("Adding hosts...")
    for hostId in hostIds:
        net.addHost('h' + str(hostId[0]), ip='10.1.0.'+str(hostId[0]), mac=hostId[1].strip())
    print("Adding links...")
    for switchId in switchLinksProperties:
        for secondSwitchId in switchLinksProperties[switchId]:
            net.addLink(net.get('sw'+str(switchId)), net.get('sw'+str(secondSwitchId)), port1=switchLinksProperties[switchId][secondSwitchId][2], port2=switchLinksProperties[switchId][secondSwitchId][3],
            bw=switchLinksProperties[switchId][secondSwitchId][0], delay=switchLinksProperties[switchId][secondSwitchId][1])
    for hostId in hostLinksProperties:
        for switchId in hostLinksProperties[hostId]:
            net.addLink(net.get('h'+str(hostId)), net.get('sw'+str(switchId)), port1=hostLinksProperties[hostId][switchId][2], port2=hostLinksProperties[hostId][switchId][3],
            bw=hostLinksProperties[hostId][switchId][0], delay=hostLinksProperties[hostId][switchId][1])
    print("Building Network...")
    net.build()

#This function reads input file and saves the information in it in dictionaries
def readLinkProperties():
    #Reading input file
    file = open("links.txt", "r")
    lines = file.readlines()
    file.close()
    switchLinksProperties = {}
    hostLinksProperties = {}
    switchIds, hostIds = set(), set()
    isInHosts = False
    isInSwitches = False
    #Going through file line by line
    for line in lines:
        parts = line.split(",")
        if(parts[0] == "hosts\n"):
            isInHosts = True
            continue
        if(parts[0] == "switches\n"):
            isInSwitches = True
            continue
        if(isInHosts and (not isInSwitches)):
            #Save information about hosts and their links to switches
            hostLinksProperties.setdefault(int(parts[0]), {})
            hostIds.add((int(parts[0]),parts[6]))
            hostLinksProperties[int(parts[0])][int(parts[1])] = (float(parts[2]), float(parts[3]), int(parts[4]), int(parts[5]))
        elif(isInSwitches):
            #Save information about switches
            switchIds.add((int(parts[0]), parts[1]))
        else:
            #Save information about links between switches
            switchLinksProperties.setdefault(int(parts[0]), {})
            switchLinksProperties[int(parts[0])][int(parts[1])] = (float(parts[2]), float(parts[3]), int(parts[4]), int(parts[5]))
    return switchIds, hostIds, switchLinksProperties, hostLinksProperties

#Starts server on each host using iperf3 -s command
def startServers():
    global net, hosts
    for nodeId in net:
        if(nodeId[0] == 'h'):
            node = net.get(nodeId)
            hosts.append(node)
            node.cmd("iperf3 -s -t 60 &")

#Stopping servers by killing the iperf running on them
def stopServers():
    global hosts
    for host in hosts:
        host.cmd("pkill iperf3")

#This function changes the link bandwidths
def changeBandwidth():
    global net, changeBandwidthTimer
    #Going through network links
    for i in range(len(net.links)):
        #Selecting new random bandwidth
        randomBandwidth = random.uniform(1, 5)
        #Changing bandwidth on both interfaces of the link
        net.links[i].intf1.params["bw"] = randomBandwidth
        net.links[i].intf2.params["bw"] = randomBandwidth
    #Printing # each time bandwidth of all links changes
    print("#", end='', flush=True)
    changeBandwidthTimer = threading.Timer(10, changeBandwidth)
    changeBandwidthTimer.start()

#In this function each host randomly selects another host and sends packet to it
def sendPackets():
    global net, sendPacketsTimer, hosts, packetsTimes
    for host in hosts:
        #Randomly select a host
        randomHost = random.choice([h for h in hosts if h != host])
        #Send packet to the host
        t1 = time.time()
        host.cmd("iperf3 -n 100000 -4 -c " + randomHost.IP())
        t2 = time.time()
        packetsTimes.write(host.IP().split(".")[-1] + "," + randomHost.IP().split(".")[-1] + "," + str(t2 - t1) + "\n")
        packetsTimes.flush()
    #Printing . each time all hosts had sent a packet to another host
    print(".", end='', flush=True)
    sendPacketsTimer = threading.Timer(0.1, sendPackets)
    sendPacketsTimer.start()

changeBandwidthTimer = None
sendPacketsTimer = None
#This function starts the timers that change bandwidth or send packets after a period of time
def startSending():
    global changeBandwidthTimer, sendPacketsTimer
    print("Starting timer to change bandwidths...")
    changeBandwidthTimer = threading.Timer(10, changeBandwidth)
    changeBandwidthTimer.start()
    print("Starting timer to send packets...")
    sendPacketsTimer = threading.Timer(0.1, sendPackets)
    sendPacketsTimer.start()
    #Wating one minute 
    time.sleep(61)
    print("\nStopping timers...")
    changeBandwidthTimer.cancel()
    sendPacketsTimer.cancel()

net = None
hosts = []
packetsTimes = open("results/packetsTimes.txt", "w+")
def start():
    global net, hosts, packetsTimes
    os.system('../mininetInstall/mininet/bin/mn -c')
    print("Reading topology file...")
    switchIds, hostIds, switchLinksProperties, hostLinksProperties = readLinkProperties()
    print("Building network...")
    buildNetworkTopology(switchIds, hostIds, switchLinksProperties, hostLinksProperties)
    print("Starting network...")
    net.start()
    print("Starting servers on hosts...")
    # CLI(net)
    startServers()
    startSending()
    print("Stopping network...")
    stopServers()
    net.stop()
    packetsTimes.close()

if __name__ == '__main__':
    start()
