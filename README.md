<h1 align="center"> Dynamic Routing and Forwarding in SDNs using MST Algorithm </h1>
<p>
    This project was undertaken within the undergraduate Computer Networks course. It was the only successful project among every other student's, and it was used for teaching in the next semester due to its flexibility and comprehensiveness. This project was implemented using Mininet and Ryu Controller.
</p>
