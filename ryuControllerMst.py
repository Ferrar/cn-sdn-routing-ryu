from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import MAIN_DISPATCHER, CONFIG_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.lib.mac import haddr_to_bin
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.lib import dpid as dpid_lib
from ryu.lib import stplib


delay = lambda x: 0.8 / float(x)
switchTopo = {}
hostTopo = {}
paths = {}

def dijkstra(src, x, dest, visited, distances, predecessors):
    global switchTopo
    if x == dest:
        path = ""
        temp = dest
        while(temp != src):
            path += temp + "-"
            temp = predecessors[temp]
        path += src
        return path[::-1]
    else:
        for neighbor in switchTopo[x]:
            if neighbor not in visited:
                new_distance = distances[x] + switchTopo[x][neighbor][0]
                if new_distance < distances[neighbor]:
                    distances[neighbor] = new_distance
                    predecessors[neighbor] = x
        visited.append(x)
        minimum = float("inf")
        minimumNode = None
        for k in switchTopo:
            if((k not in visited) and (distances[k] < minimum)):
                minimum = distances[k]
                minimumNode = k
        return dijkstra(src, minimumNode, dest, visited, distances, predecessors)

def getDelay(path):
    delay = 0
    splitted = path.split('-')
    for i in range(len(splitted)-1):
        delay += switchTopo[splitted[i]][splitted[i+1]][0]
    return delay

class RyuController(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]
    _CONTEXTS = {'stplib': stplib.Stp}

    def __init__(self, *args, **kwargs):
        super(RyuController, self).__init__(*args, **kwargs)
        self.dijkstraCalculated = False
        self.topology_api_app = self
        self.mac_to_port = {}
        self.initializeTopology()

    def add_flow(self, datapath, priority, match, actions, buffer_id=None):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,actions)]
        if buffer_id:
            mod = parser.OFPFlowMod(datapath=datapath, buffer_id=buffer_id,priority=priority, match=match,instructions=inst)
        else:
            mod = parser.OFPFlowMod(datapath=datapath, priority=priority,match=match, instructions=inst)
        datapath.send_msg(mod)

    @set_ev_cls(stplib.EventPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):
        global paths, hostTopo, switchTopo
        msg = ev.msg
        datapath = msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        pkt = packet.Packet(msg.data)
        eth = pkt.get_protocols(ethernet.ethernet)[0]
        inPort = msg.match['in_port']
        dst = eth.dst
        src = eth.src
        dpid = datapath.id
        self.mac_to_port.setdefault(dpid, {})
        self.logger.info("packet in %s %s %s %s", dpid, src, dst, inPort)
        outPort = None
        ss, dd = "-", "-"
        path = "-"
        if(dst[:15] != '00:00:00:00:00:'):
            outPort = ofproto.OFPP_FLOOD
        else:
            if(self.dijkstraCalculated == False):
                self.calculateDijkstra()
                self.dijkstraCalculated = True
            ss, srcId = self.findHostIdByMac(src)
            dd, dstId = self.findHostIdByMac(dst)
            if(srcId == dstId):
                outPort = hostTopo[dd][2]
                self.mac_to_port[dpid][src] = outPort
            else:
                path = paths[srcId][dstId].split('-')
                if(str(dpid) == path[-1]):
                    outPort = hostTopo[dd][2]
                    self.mac_to_port[dpid][src] = outPort
                else:
                    for i in range(len(path) - 1):
                        if(path[i] == str(dpid)):
                            for sw1 in switchTopo:
                                for sw2 in switchTopo[sw1]:
                                    if((sw1 == path[i]) and (sw2 == path[i+1])):
                                        outPort = switchTopo[sw1][sw2][1]
                                        self.mac_to_port[dpid][src] = outPort
                                        break
        actions = [parser.OFPActionOutput(outPort)]
        if outPort != ofproto.OFPP_FLOOD:
            # print("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
            # print(outPort, inPort, dst, src)
            # print(type(outPort), type(inPort), type(dst), type(src))
            # print("flows data: ", inPort, ss, dd, outPort, dpid, path)
            # print("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
            match = parser.OFPMatch(in_port=inPort, eth_dst=dst)
            self.add_flow(datapath, 1, match, actions)
        data = None
        if msg.buffer_id == ofproto.OFP_NO_BUFFER:
            data = msg.data
        out = parser.OFPPacketOut(datapath=datapath, buffer_id=msg.buffer_id,in_port=inPort, actions=actions, data=data)
        datapath.send_msg(out)

    def findHostIdByMac(self, mac):
        global hostTopo
        for h in hostTopo:
            if(hostTopo[h][3].strip() == mac.strip()):
                return h, hostTopo[h][0]

    def initializeTopology(self):
        global switchTopo, hostTopo, delay
        with open('links.txt', 'r') as f:
            isHostLinks = False
            isInSwitches = False
            lines = f.readlines()
            for line in lines:
                line = line.strip()
                if line == 'hosts':
                    isHostLinks = True
                    continue
                if line == 'switches':
                    isInSwitches = True
                    continue
                lineSp = line.split(',')
                if not isHostLinks:
                    switchTopo.setdefault(lineSp[0], {})
                    switchTopo.setdefault(lineSp[1], {})
                    switchTopo[lineSp[0]][lineSp[1]] = (delay(lineSp[2]) + float(lineSp[3]), int(lineSp[4]))
                    switchTopo[lineSp[1]][lineSp[0]] = (delay(lineSp[2]) + float(lineSp[3]), int(lineSp[5]))
                elif not isInSwitches:
                    hostTopo.setdefault(lineSp[0], [])
                    hostTopo[lineSp[0]].append(lineSp[1])
                    hostTopo[lineSp[0]].append(delay(lineSp[2]) + float(lineSp[3]))
                    hostTopo[lineSp[0]].append(int(lineSp[5]))
                    hostTopo[lineSp[0]].append(lineSp[6])
                else:
                    pass

    def calculateDijkstra(self):
        global switchTopo, hostTopo, paths
        switches = [int(switch) for switch in switchTopo]
        for i in range(len(switches) - 1):
            for j in range(i+1, len(switches)):
                srcId = str(switches[i])
                dstId = str(switches[j])
                distances = {}
                predecessors = {}
                for switch in switchTopo:
                    distances[switch] = float("inf")
                    predecessors[switch] = "-"
                distances[srcId] = 0
                p = dijkstra(srcId, srcId, dstId, [], distances, predecessors)
                paths.setdefault(srcId, {})
                paths.setdefault(dstId, {})
                paths[srcId][dstId] = p
                paths[dstId][srcId] = p[::-1]
        print('Dijkstra paths between switches:')
        for i in switches:
            for j in paths[str(i)]:
                print('From {} to {}: path -> {} , cost = {}'.format(str(i), j, paths[str(i)][j], getDelay(paths[str(i)][j])))
    
    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def switch_features_handler(self, ev):
        datapath = ev.msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        match = parser.OFPMatch()
        actions = [parser.OFPActionOutput(ofproto.OFPP_CONTROLLER,ofproto.OFPCML_NO_BUFFER)]
        self.add_flow(datapath, 0, match, actions)