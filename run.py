import os
import matplotlib.pyplot as plt

def plot(h1, h2, title, yValues):
    _, ax = plt.subplots(1, 1, figsize=(16, 8))
    xValues = [(i+1) for i in range(len(yValues))]
    ax.plot(xValues, yValues, color="blue")
    ax.set_xlabel('Packets', labelpad=9, size=14)
    ax.set_ylabel('Send Duration', labelpad=9, size=14)
    ax.set_title(title, pad=9, weight='bold', size=14)
    plt.savefig("./results/plots/" + h1 + "_" + h2)

os.system("sudo rm -rf ./results/plots")
os.mkdir("./results/plots")
packetTimesFile = open("results/packetsTimes.txt", "r")
lines = packetTimesFile.readlines()
durationsDict = {}

for line in lines:
    line = line.strip()
    splitted = line.split(",")
    src = splitted[0]
    dst = splitted[1]
    duration = float(splitted[2])
    durationsDict.setdefault(src, {})
    durationsDict[src].setdefault(dst, [])
    durationsDict[src][dst].append(duration)

for h1 in durationsDict:
    for h2 in durationsDict[h1]:
        plot(h1, h2, "Packets sending duration from hosts h{} to h{}".format(h1, h2), durationsDict[h1][h2])
